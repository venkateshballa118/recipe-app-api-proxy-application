# reciepe app api proxy

NGINX proxy app for our recipe app API

## Usage

### Environment Variable

* `LISTEN_PORT` -  Port to listen* on (default: `8000`)
* `APP_HOST` - Hostname of the app to forword request to (default: `app`)
* `APP_PORT` - Port of the app to forword requests to (default: `9000`)
